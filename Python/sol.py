import pandas as pd
import matplotlib.pyplot as plt
import math

def getDistance(a, b):
    return math.sqrt(math.pow(a[0]-b[0],2)+math.pow(a[1]-b[1],2))

def bestPoint(p):
    dist_red = getDistance(p, red)
    dist_green = getDistance(p, green)
    dist_blue = getDistance(p, blue)
    if dist_red < dist_green:
        if dist_red < dist_blue:
            return "red"
        else:
            return "blue"
    else:
        if dist_green < dist_blue:
            return "green"
        else:
            return "blue"

def checkConstraints():
    if red_max < len(red_locs) or len(red_locs) < red_min:
        return False
    if green_max < len(green_locs) or len(green_locs) < green_min:
        return False
    if blue_max < len(blue_locs) or len(blue_locs) < blue_min:
        return False
    else:
        return True

def fixConstraints():
    while len(red_locs) > red_max:
        if len(green_locs) < green_max:
            if len(blue_locs) < blue_max:
                imp = getMinImpactTransfer(red_locs, [green_locs, blue_locs])
                transfer(imp[1], red_locs, imp[2])
            else:
                imp = getMinImpactTransfer(red_locs, [green_locs])
                transfer(imp[1], red_locs, imp[2])
        else:
            if len(blue_locs) < blue_max:
                imp = getMinImpactTransfer(red_locs, [blue_locs])
                transfer(imp[1], red_locs, imp[2])
            else:
                pass # TODO Error in this case not solvable.
    while len(green_locs) > green_max:
        if len(red_locs) < red_max:
            if len(blue_locs) < blue_max:
                imp = getMinImpactTransfer(green_locs, [red_locs, blue_locs])
                transfer(imp[1], green_locs, imp[2])
            else:
                imp = getMinImpactTransfer(green_locs, [red_locs])
                transfer(imp[1], green_locs, imp[2])
        else:
            if len(blue_locs) < blue_max:
                imp = getMinImpactTransfer(green_locs, [blue_locs])
                transfer(imp[1], green_locs, imp[2])
            else:
                pass # TODO Error in this case not solvable.
    while len(blue_locs) > blue_max:
        if len(green_locs) < green_max:
            if len(red_locs) < red_max:
                imp = getMinImpactTransfer(blue_locs, [green_locs, red_locs])
                transfer(imp[1], blue_locs, imp[2])
            else:
                imp = getMinImpactTransfer(red_locs, [green_locs])
                transfer(imp[1], blue_locs, imp[2])
        else:
            if len(blue_locs) < blue_max:
                imp = getMinImpactTransfer(red_locs, [blue_locs])
                transfer(imp[1], blue_locs, imp[2])
            else:
                pass # TODO Error in this case not solvable.

    while len(red_locs) < red_min:
        if len(green_locs) > green_min:
            if len(blue_locs) > blue_min:
                imp_g = getMinImpactTransfer(green_locs, [red_locs])
                imp_b = getMinImpactTransfer(blue_locs, [red_locs])
                if imp_g[0]<imp_b[0]:
                    transfer(imp_g[1], imp_g[2], red_locs)
                else:
                    transfer(imp_b[1], imp_b[2], red_locs)
            else:
                imp_g = getMinImpactTransfer(green_locs, [red_locs])
                transfer(imp_g[1], imp_g[2], red_locs)
        else:
            if len(blue_locs) > blue_min:
                imp_b = getMinImpactTransfer(blue_locs, [red_locs])
                transfer(imp_b[1], imp_b[2], red_locs)
            else:
                pass # TODO Error in this case not solvable.
    while len(green_locs) < green_min:
        if len(red_locs) > red_min:
            if len(blue_locs) > blue_min:
                imp_r = getMinImpactTransfer(red_locs, [green_locs])
                imp_b = getMinImpactTransfer(blue_locs, [green_locs])
                if imp_g[0]<imp_b[0]:
                    transfer(imp_r[1], imp_r[2], green_locs)
                else:
                    transfer(imp_b[1], imp_b[2], green_locs)
            else:
                imp_g = getMinImpactTransfer(red_locs, [green_locs])
                transfer(imp_r[1], imp_r[2], green_locs)
        else:
            if len(blue_locs) > blue_min:
                imp_b = getMinImpactTransfer(blue_locs, [green_locs])
                transfer(imp_b[1], imp_b[2], green_locs)
            else:
                pass # TODO Error in this case not solvable.
    while len(blue_locs) < blue_min:
        if len(green_locs) > green_min:
            if len(red_locs) > red_min:
                imp_g = getMinImpactTransfer(green_locs, [blue_locs])
                imp_r = getMinImpactTransfer(red_locs, [blue_locs])
                if imp_g[0]<imp_r[0]:
                    transfer(imp_g[1], imp_g[2], blue_locs)
                else:
                    transfer(imp_r[1], imp_b[2], blue_locs)
            else:
                imp_g = getMinImpactTransfer(green_locs, [blue_locs])
                transfer(imp_g[1], imp_g[2], blue_locs)
        else:
            if len(blue_locs) > blue_min:
                imp_r = getMinImpactTransfer(red_locs, [blue_locs])
                transfer(imp_r[1], imp_r[2], blue_locs)
            else:
                pass # TODO Error in this case not solvable.

def optimize():
    while getMinImpact(red_locs, green_locs)<0:
        swapMinImpact(red_locs,green_locs)
    while getMinImpact(red_locs, blue_locs)<0:
        swapMinImpact(red_locs,blue_locs)
    while getMinImpact(green_locs, blue_locs)<0:
        swapMinImpact(green_locs,blue_locs)
    while getMinImpactTransfer(red_locs, [green_locs])[0]<0 and len(red) > red_min and len(green_locs) < green_max:
        imp = getMinImpactTransfer(red_locs, [green_locs])
        transfer(imp[1], red_locs, imp[2])
    while getMinImpactTransfer(red_locs, [blue_locs])[0]<0 and len(red) > red_min and len(blue_locs) < blue_max:
        imp = getMinImpactTransfer(red_locs, [blue_locs])
        transfer(imp[1], red_locs, imp[2])
    while getMinImpactTransfer(blue_locs, [green_locs])[0]<0 and len(blue) > blue_min and len(green_locs) < green_max:
        imp = getMinImpactTransfer(blue_locs, [green_locs])
        transfer(imp[1], blue_locs, imp[2])
    while getMinImpactTransfer(blue_locs, [red_locs])[0]<0 and len(blue) > blue_min and len(red_locs) < red_max:
        imp = getMinImpactTransfer(blue_locs, [red_locs])
        transfer(imp[1], blue_locs, imp[2])
    while getMinImpactTransfer(green_locs, [blue_locs])[0]<0 and len(green) > green_min and len(blue_locs) < blue_max:
        imp = getMinImpactTransfer(green_locs, [blue_locs])
        transfer(imp[1], green_locs, imp[2])
    while getMinImpactTransfer(green_locs, [red_locs])[0]<0 and len(green) > green_min and len(red_locs) < red_max:
        imp = getMinImpactTransfer(green_locs, [red_locs])
        transfer(imp[1], green_locs, imp[2])



def score():
    red_score = 0
    green_score = 0
    blue_score = 0
    for loc in red_locs:
        red_score+=getDistance(loc, red)
    for loc in green_locs:
        green_score+=getDistance(loc, green)
    for loc in blue_locs:
        blue_score+=getDistance(loc, blue)
    return red_score+green_score+blue_score

def convertArrToPoint(a):
    if a == red_locs:
        return red
    if a == green_locs:
        return green
    if a == blue_locs:
        return blue

def transfer(a, b, c):
    b.remove(a)
    c.append(a)

def getMinImpactTransfer(a, b):
    min_imp = 1000
    a_o = None
    t_a = None
    for o in a:
        dist_a_o = getDistance(o, convertArrToPoint(a))
        for t in b:
            dist_o_t = getDistance(o, convertArrToPoint(t))
            imp = dist_o_t-dist_a_o
            if imp < min_imp:
                a_o = o
                t_a = t
                min_imp = imp
    return [min_imp, a_o, t_a]

def getMinImpact(a, b):
    min_imp = 1000
    a_o = None
    b_o = None
    for o in a:
        dist_a_o = getDistance(o, convertArrToPoint(a))
        dist_b_o = getDistance(o, convertArrToPoint(b))
        for t in b:
            dist_b_t = getDistance(t, convertArrToPoint(b))
            dist_a_t = getDistance(t, convertArrToPoint(a))
            imp = dist_a_t+dist_b_o-dist_a_o-dist_b_t
            if imp < min_imp:
                a_o = o
                b_o = t
                min_imp = imp
    return min_imp

def swapMinImpact(a, b):
    min_imp = 1000
    a_o = None
    b_o = None
    for o in a:
        for t in b:
            dist_a_o = getDistance(o, convertArrToPoint(a))
            dist_b_t = getDistance(t, convertArrToPoint(b))
            dist_a_t = getDistance(t, convertArrToPoint(a))
            dist_b_o = getDistance(o, convertArrToPoint(b))
            imp = dist_a_t+dist_b_o-dist_a_o-dist_b_t
            if imp < min_imp:
                a_o = o
                b_o = t
                min_imp = imp
    a.remove(a_o)
    b.remove(b_o)
    a.append(b_o)
    b.append(a_o)

raw_data = pd.read_csv('data.csv', delimiter=';', header=None)
red = [41.049792, 29.003031]
red_locs = list()
red_min = 20
red_max = 30
green = [41.069940, 29.019250]
green_locs = list()
green_min = 35
green_max = 50
blue = [41.049997, 29.026108]
blue_locs = list()
blue_min = 20
blue_max = 80
print(raw_data)

locations = list()
for index, row in raw_data.iterrows():
    locations.append([row[1],row[2]])

for loc in locations:
    group = bestPoint(loc)
    if group == "red":
        loc.append(getDistance(loc, red))
        red_locs.append(loc)
    if group == "green":
        loc.append(getDistance(loc, green))
        green_locs.append(loc)
    if group == "blue":
        loc.append(getDistance(loc, blue))
        blue_locs.append(loc)

print(len(red_locs))
print(len(green_locs))
print(len(blue_locs))

while not checkConstraints():
    fixConstraints()

print(len(red_locs))
print(len(green_locs))
print(len(blue_locs))

print(score())

scr = 0

while scr != score():
    scr = score()
    optimize()
    print(len(red_locs))
    print(len(green_locs))
    print(len(blue_locs))
    print(score())

plt.scatter([row[0] for row in red_locs], [row[1] for row in red_locs], color="red")
plt.scatter([row[0] for row in green_locs], [row[1] for row in green_locs], color="green")
plt.scatter([row[0] for row in blue_locs], [row[1] for row in blue_locs], color="blue")
plt.scatter(red[0],red[1], color='red', marker='x')
plt.scatter(green[0],green[1], color='green', marker='x')
plt.scatter(blue[0],blue[1], color='blue', marker='x')
plt.show()

package util

import model.Color
import model.Order
import model.Vendor
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class DataReader{

    companion object {
        fun readOrderData(file: File) : ArrayList<Order>{
            val sc = Scanner(file)
            val orders = ArrayList<Order>()
            while (sc.hasNextLine()) {
                val tokens = sc.nextLine().split(";")
                orders.add(Order(tokens[0].toInt(), tokens[1].toDouble(), tokens[2].toDouble(), Color.BLACK))
            }
            return orders
        }
        fun readVendorData(file: File) : ArrayList<Vendor>{
            val sc = Scanner(file)
            val vendors = ArrayList<Vendor>()
            while(sc.hasNextLine()) {
                val tokens = sc.nextLine().split(";")
                vendors.add(Vendor(Color.valueOf(tokens[0]), tokens[1].toDouble(), tokens[2].toDouble(),
                        tokens[3].toInt(), tokens[4].toInt()))
            }
            return vendors
        }
    }

}
package main

import model.Color
import model.Problem
import model.Vendor
import util.DataReader
import java.io.File

fun main(args: Array<String>){

    val orders = DataReader.readOrderData(File("./data/order_coordinates.csv"))
    val vendors = DataReader.readVendorData(File("./data/vendor_coordinates.csv"))

    val problem = Problem(orders, vendors)
    problem.initialize()
    println(problem.vendors)
    println(problem.orders)
    //while (!problem.validate())
    //    problem.fixConstraints()
}
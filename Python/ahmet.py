import pandas as pd
import matplotlib.pyplot as plt
import math
from sklearn.cluster import AgglomerativeClustering

def getDistance(a, b):
    return math.sqrt(math.pow(a[0]-b[0],2)+math.pow(a[1]-b[1],2))

raw_data = pd.read_csv('data.csv', delimiter=';', header=None)
red = [41.049792, 29.003031]
green = [41.069940, 29.019250]
blue = [41.049997, 29.026108]
print(raw_data)

locations = list()
for index, row in raw_data.iterrows():
    #parsed = row.split(';')
    locations.append([row[1],row[2]])

print(locations)
plt.scatter(raw_data[1].tolist(), raw_data[2].tolist(), color='black')
plt.scatter(red[0],red[1], color='red', marker='x')
plt.scatter(green[0],green[1], color='green', marker='x')
plt.scatter(blue[0],blue[1], color='blue', marker='x')

green_distances = {}
for location in locations:
    dist = getDistance(location, green) - min(getDistance(location, blue), getDistance(location, red))
    green_distances[dist] = location

keys = list(green_distances.keys())

keys.sort()

for key in keys[:35]:
    point = green_distances[key]
    plt.scatter(point[0], point[1], color='green')
    locations.remove(point)

blue_distances = {}

for location in locations:
    dist = getDistance(location, blue) - min(getDistance(location, green), getDistance(location, red))
    blue_distances[dist] = location

keys = list(blue_distances.keys())
keys.sort()

for key in keys[:20]:
    point = blue_distances[key]
    plt.scatter(point[0], point[1], color='blue')
    locations.remove(point)

red_distances = {}

for location in locations:
    dist = getDistance(location, red) - min(getDistance(location, green), getDistance(location, blue))
    red_distances[dist] = location

keys = list(red_distances.keys())
keys.sort()


for key in keys[:20]:
    point = red_distances[key]
    plt.scatter(point[0], point[1], color='red')
    locations.remove(point)


plt.show()

for loc in locations:
    d1 = getDistance(loc, red)
    d2 = getDistance(loc, green)
    d3 = getDistance(loc, blue)




package model

enum class Color {
    RED, GREEN, BLUE, BLACK
}
package model

interface Locatable {
    val x: Double
    val y: Double
}
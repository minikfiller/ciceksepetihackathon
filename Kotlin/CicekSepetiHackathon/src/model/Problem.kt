package model

import util.SolverUtil
import util.SolverUtil.Companion.getClosestVendor
import util.SolverUtil.Companion.getDistance

data class Problem(val orders: ArrayList<Order>, val vendors: ArrayList<Vendor>) {

    fun validate() = vendors.map{it.orders.size<it.max && it.orders.size>it.min}.reduce{acc, it -> acc && it}
    fun score() = vendors.map{vendor -> vendor.orders.sumBy{order -> SolverUtil.getDistance(order, vendor).toInt()}}
    fun initialize() = orders.forEach{order ->
        val closestVendor = getClosestVendor(order, vendors)!!
        closestVendor.orders.add(order)
        order.c = closestVendor.c
        println(order)
     }
    fun fixConstraints(){

    }

}
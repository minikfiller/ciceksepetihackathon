package util

import model.Locatable
import model.Order
import model.Vendor

class SolverUtil {
    companion object {
        fun getDistance(l1: Locatable, l2: Locatable) = Math.sqrt(Math.pow(l1.x-l2.x,2.0)+Math.pow(l1.y-l2.y,2.0))
        fun getClosestVendor(o: Order, vendorList: ArrayList<Vendor>) = (vendorList.minWith(compareBy {getDistance(o, it)}))
        fun swapVendor(from: Vendor, order: Order, to: Vendor) = from.orders.remove(order).and(to.orders.add(order))
        fun swapOrders(v1: Vendor, v2: Vendor) = v1.orders.map{o1 -> Pair<Order?,Order?>(o1,v2.orders.minBy{o2 -> getDistance(o1,o2)})}.minBy{getDistance(it.first!!,it.second!!)}.apply{if(swapFeasible(v1, this!!.first!!, v2, this!!.second!!))v1.orders.remove(this!!.first!!).and(v1.orders.add(this!!.second!!)).and(v2.orders.remove(this!!.second!!)).and(v2.orders.add(this!!.first!!))}
        fun swapFeasible(v1: Vendor, o1: Order, v2: Vendor, o2: Order) = 0<(getDistance(o1,v1)+getDistance(o2, v2)-getDistance(o1, v2)-getDistance(o2, v1))
    }
}
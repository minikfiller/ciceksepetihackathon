package model

data class Vendor(
    val c: Color,
    override val y: Double,
    override val x: Double,
    val min: Int,
    val max: Int
) : Locatable {
    val orders = ArrayList<Order>()
}
import java.io.BufferedReader
import java.io.FileReader

/*
    -Çiçek bayilerinin kat edeceği toplam kuş uçuşu mesafenin olabildiğince kısa olması gerekiyor.
    -Her çiçek bayisine atanan çiçek paketinin harita üzerinde ilgili bayi rengiyle işaretlenmiş olması gerekiyor.
 */

fun main(args: Array<String>) {
    var fileReader: BufferedReader?
    val orders = ArrayList<Order>()
    val vendors = ArrayList<Vendor>()
    try {
        var line: String?
        /** Read orders */
        fileReader = BufferedReader(FileReader("./data/order_coordinates.csv"))

        // Read CSV header
        fileReader.readLine()
        // Read the file line by line starting from the second line
        line = fileReader.readLine()
        while (line != null) {
            val tokens = line.split(",")
            if (tokens.isNotEmpty()) {
                val order = Order(
                        tokens[0].toInt(),
                        tokens[1].toDouble(),
                        tokens[2].toDouble())
                orders.add(order)
            }
            line = fileReader.readLine()
        }

        fileReader.close()
        /** Read vendors */
        fileReader = BufferedReader(FileReader("./data/vendor_coordinates.csv"))

        // Read CSV header
        fileReader.readLine()
        // Read the file line by line starting from the second line
        line = fileReader.readLine()
        while (line != null) {
            val tokens = line.split(",")
            if (tokens.isNotEmpty()) {
                val vendor = Vendor(
                        tokens[0],
                        tokens[1].toDouble(),
                        tokens[2].toDouble(),
                        tokens[3].toInt(),
                        tokens[4].toInt())
                vendors.add(vendor)
            }
            line = fileReader.readLine()
        }
        fileReader.close()
    } catch (e: Exception) {
        println("Reading CSV Error!")
        e.printStackTrace()
    }

    for (vendor:Vendor in vendors){
        for (i in 0..vendor.minOrder){

        }
    }

}

// Order POJO
class Order(var orderNo: Int, var latitude: Double, var longitude: Double) {
    var visited: Boolean = false
    var color : String = ""

    override fun toString(): String {
        return "Order(orderNo=$orderNo, latitude=$latitude, longitude=$longitude)"
    }

    fun distanceTo(order: Order): Double {
        return Math.pow(this.latitude - order.latitude, 2.0) + Math.pow(this.longitude - order.longitude, 2.0)
    }

}

// Vendor POJO
class Vendor(var vendorName: String, var latitude: Double, var longitude: Double, var minOrder: Int, var maxOrder: Int) {

    var numOrder: Int = 0

    override fun toString(): String {
        return "Vendor(vendorName=$vendorName, latitude=$latitude, longitude=$longitude, minOrder=$minOrder, maxOrder=$maxOrder)"
    }

    fun distanceTo(order: Order): Double {
        return Math.pow(this.latitude - order.latitude, 2.0) + Math.pow(this.longitude - order.longitude, 2.0)
    }


    fun nextOrder(order: Order?, otherOrders: List<Order>): Order {
        var minDistance: Double = Double.MAX_VALUE
        var next: Order = otherOrders[0]

        for (otherOrder: Order in otherOrders){
            var distance: Double = 0.0
            if (order != null){
                distance += order.distanceTo(otherOrder)
            }
            distance += this.distanceTo(otherOrder);

            if (distance < minDistance) {
                next = otherOrder
                minDistance = distance
            }
        }

        return next
    }

}


fun magic(list : ArrayList<Order>, vendors : ArrayList<Order>) {
    for (i in 0..99) {
        val dist1 = list[i].distanceTo(vendors[0])
        val dist2 = list[i].distanceTo(vendors[1])
        val dist3 = list[i].distanceTo(vendors[2])
        val dists: DoubleArray = doubleArrayOf(dist1, dist2, dist3)
        dists.sort()
        /*
        var vendor = dists[0]
        if (vendor.numOrder < vendor.maxOrder){
            vendor.numOrder ++
        }
        else {
            if (dists[1].numOrder < dists[1].maxOrder){
                vendor = dists[1]
                vendor.numOrder ++
            }
            else{
                if (dists[2].numOrder < dists[2].maxOrder){
                    vendor = dists[2]
                    vendor.numOrder ++
                }
            }
        }
        orders[i].color = vendor.vendorName
        */
    }
}